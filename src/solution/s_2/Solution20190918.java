package solution.s_2;

class ListNode {
    int val;
    ListNode next;
    ListNode(int x) { val = x; }
}

public class Solution20190918 {
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        return add(l1, l2, 0);
    }

    private ListNode add(ListNode listNode1, ListNode listNode2, int carry) {
        if (listNode1 == null && listNode2 == null) {
            return carry == 0 ? null : new ListNode(carry);
        }

        int l1Value = (listNode1 == null) ? 0 : listNode1.val;
        int l2Value = (listNode2 == null) ? 0 : listNode2.val;
        int sum = l1Value + l2Value + carry;
        ListNode curNode = new ListNode(sum % 10);
        curNode.next  = add(
                listNode1 == null ? null : listNode1.next,
                listNode2 == null ? null : listNode2.next,
                sum > 9 ? 1 : 0);
        return curNode;
    }
}
