package solution.s_3;

public class Main {
    public static void main(String[] args) {
        Solution20200921 solution = new Solution20200921();
        System.out.println(solution.lengthOfLongestSubstring("abcabcbb"));
        System.out.println(solution.lengthOfLongestSubstring("bbbbb"));
        System.out.println(solution.lengthOfLongestSubstring("pwwkew"));
    }
}
