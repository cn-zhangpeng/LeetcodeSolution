package solution.s_129;

public class Main {
    public static void main(String[] args) {
        Solution20201029 solution = new Solution20201029();

        TreeNode node1 = new TreeNode(1);
        TreeNode node2 = new TreeNode(2);
        TreeNode node3 = new TreeNode(3);
        node1.left = node2;
        node1.right = node3;
        System.out.println(solution.sumNumbers(node1));

        TreeNode node4 = new TreeNode(4);
        TreeNode node5 = new TreeNode(9);
        TreeNode node6 = new TreeNode(0);
        TreeNode node7 = new TreeNode(5);
        TreeNode node8 = new TreeNode(1);
        node4.left = node5;
        node4.right = node6;
        node5.left = node7;
        node5.right = node8;
        System.out.println(solution.sumNumbers(node4));

    }
}
