package solution.s_941;

public class Main {
    public static void main(String[] args) {
        Solution20201103 solution = new Solution20201103();

        int[] nums1 = new int[] {2, 1};
        System.out.println(solution.validMountainArray(nums1));

        int[] nums2 = new int[] {3, 5, 5};
        System.out.println(solution.validMountainArray(nums2));

        int[] nums3 = new int[] {0, 3, 2, 1};
        System.out.println(solution.validMountainArray(nums3));

        int[] nums4 = new int[] {14,82,89,84,79,70,70,68,67,66,63,60,58,54,44,43,32,28,26,25,22,15,13,12,10,8,7,5,4,3};
        System.out.println(solution.validMountainArray(nums4));
    }
}
