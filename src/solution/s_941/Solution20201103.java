package solution.s_941;

/**
 * 解题思路：
 *      定义 m 表示峰值，初始值为 -1。
 *      首先 m 应该满足 m != -1 && m != 0 && m != A.length-1 。
 *      然后判断 [m, A.length) 的元素，是否满足 A[i] > A[i+1]。
 */
public class Solution20201103 {
    public boolean validMountainArray(int[] A) {
        // 元素小于 3，不成峰
        if (A.length < 3) { return false; }

        // 初始值为 -1
        int m = -1;
        for (int i = 0; i < A.length - 1; i ++) {
            // 假如两个元素相等，不成峰
            if (A[i] == A[i + 1]) {
                return false;
            }

            // 假如当前元素大于下一个元素，说明当前元素是峰值
            if (A[i] > A[i + 1]) {
                m = i;
                break;
            }
        }

        // 假如 m 等于如下值，不成峰
        if (m == -1 || m == 0 || m == A.length - 1) {
            return false;
        }

        // 判断 [m, A.length) 元素，假如出现 A[i] <= A[i+1] 的情况，不成峰
        for (int i = m; i < A.length - 1; i ++) {
            if (A[i] <= A[i + 1]) {
                return false;
            }
        }

        return true;
    }
}
