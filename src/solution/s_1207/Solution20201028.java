package solution.s_1207;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * 解题思路：
 *      遍历数组，将元素与出现的次数保存到 map 中。key：元素，value：出现次数！
 *      然后将 map 中的 value 存放到 set 中。
 *      最后假如 map 的大小与 set 的大小相同，说明出现次数是独一无二的。返回 true；否则返回 false。
 */
public class Solution20201028 {
    public boolean uniqueOccurrences(int[] arr) {
        if (arr.length <= 0) { return true; }

        Map<Integer, Integer> map = new HashMap<>();
        for (int num : arr) {
            map.put(num, map.getOrDefault(num, 0) + 1);
        }

        Set<Integer> set = new HashSet<>(map.values());
        return map.size() == set.size();
    }
}
