package solution.s_1207;

public class Main {
    public static void main(String[] args) {
        Solution20201028 solution = new Solution20201028();

        int[] nums1 = new int[] {1,2,2,1,1,3};
        System.out.println(solution.uniqueOccurrences(nums1));

        int[] nums2 = new int[] {1,2};
        System.out.println(solution.uniqueOccurrences(nums2));

        int[] nums3 = new int[] {-3,0,1,-3,1,1,1,-3,10,0};
        System.out.println(solution.uniqueOccurrences(nums3));
    }
}
