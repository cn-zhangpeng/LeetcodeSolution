package solution.s_57;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Solution20201104 solution = new Solution20201104();

        int[][] intervals1 = new int[][] {{3,4}};
        int[] newInterval1 = new int[] {1,2};
        System.out.println(Arrays.deepToString(solution.insert(intervals1, newInterval1)));

        int[][] intervals2 = new int[][] {{1,2}};
        int[] newInterval2 = new int[] {5,6};
        System.out.println(Arrays.deepToString(solution.insert(intervals2, newInterval2)));

        int[][] intervals3 = new int[][] {{1,3}, {6, 9}};
        int[] newInterval3 = new int[] {2,5};
        System.out.println(Arrays.deepToString(solution.insert(intervals3, newInterval3)));

        int[][] intervals4 = new int[][] {{1,2}, {3,5}, {6,7}, {8,10}, {12,16}};
        int[] newInterval4 = new int[] {4,8};
        System.out.println(Arrays.deepToString(solution.insert(intervals4, newInterval4)));

        int[][] intervals5 = new int[][] {{1,5}};
        int[] newInterval5 = new int[] {5,7};
        System.out.println(Arrays.deepToString(solution.insert(intervals5, newInterval5)));

        int[][] intervals6 = new int[][] {{2,4}, {5,7}, {8,10}, {11,13}};
        int[] newInterval6 = new int[] {3,6};
        System.out.println(Arrays.deepToString(solution.insert(intervals6, newInterval6)));
    }
}
