package solution.s_1;

public class Solution20190918 {
    public int[] twoSum(int[] nums, int target) {
        int[] result = new int[2];
        Outer: for (int i = 0; i < nums.length; i ++) {
            for (int j = 1; j < nums.length && j != i; j ++) {
                if (nums[i] + nums[j] == target) {
                    result[0] = i;
                    result[1] = j;
                    break Outer;
                }
            }
        }
        return result;
    }
}
