package solution.s_328;

public class Main {
    public static void main(String[] args) {
        Solution20201113 solution = new Solution20201113();

        ListNode head1 = constructListNode(new int[] {1, 2, 3, 4, 5});
        printNode(solution.oddEvenList(head1));

        ListNode head2 = constructListNode(new int[] {2, 1, 3, 5, 6, 4, 7});
        printNode(solution.oddEvenList(head2));
    }

    private static ListNode constructListNode(int[] vals) {
        if (vals.length <= 0) { return null; }

        ListNode head = new ListNode(vals[0]), p = head;
        for (int i = 1; i < vals.length; i ++) {
            p.next = new ListNode(vals[i]);
            p = p.next;
        }

        return head;
    }

    private static void printNode(ListNode head) {
        while (head != null) {
            System.out.printf("%d", head.val);
            if (head.next != null) {
                System.out.print("->");
            }
            head = head.next;
        }
        System.out.println();
    }
}
