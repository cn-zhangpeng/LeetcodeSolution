package solution.s_9;

public class Main {
    public static void main(String[] args) {
        System.out.println(new Solution20190918().isPalindrome(121));
        System.out.println(new Solution20190918().isPalindrome(-121));
        System.out.println(new Solution20190918().isPalindrome(10));
    }
}
