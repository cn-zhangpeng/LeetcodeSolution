package solution.s_7;

public class Main {
    public static void main(String[] args) {
        System.out.println(new Solution20190918().reverse(123));
        System.out.println(new Solution20190918().reverse(-123));
        System.out.println(new Solution20190918().reverse(120));
    }
}
