package solution.s_7;

public class Solution20190918 {
    public int reverse(int x) {
        long temp = 0;
        while (x != 0) {
            int cur = x % 10;
            temp = temp * 10 + cur;
            if (temp > Integer.MAX_VALUE || temp < Integer.MIN_VALUE) {
                return 0;
            }
            x /= 10;
        }
        return (int) temp;
    }
}
