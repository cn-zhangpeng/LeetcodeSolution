package solution.s_20;

import java.util.Stack;

public class Solution20190918 {
    public boolean isValid(String s) {
        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < s.length(); i ++) {
            char c1 = s.charAt(i);
            if (c1 == '(' || c1 == '[' || c1 == '{') {
                stack.push(c1);
                continue;
            }

            if (stack.isEmpty()) return false;

            char c2 = stack.pop();
            if (c1 == ')' && c2 != '(') return false;
            if (c1 == ']' && c2 != '[') return false;
            if (c1 == '}' && c2 != '{') return false;
        }
        return stack.isEmpty();
    }
}
