package solution.s_20;

public class Main {
    public static void main(String[] args) {
        System.out.println(new Solution20190918().isValid("()"));
        System.out.println(new Solution20190918().isValid("()[]{}"));
        System.out.println(new Solution20190918().isValid("(]"));
        System.out.println(new Solution20190918().isValid("([)]"));
        System.out.println(new Solution20190918().isValid("{[]}"));
    }
}
