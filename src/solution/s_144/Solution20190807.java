package solution.s_144;

import java.util.ArrayList;
import java.util.List;

/**
 * 递归实现
 */
public class Solution20190807 {
    public List<Integer> preorderTraversal(TreeNode root) {
        return preorderTraversal(root, new ArrayList<>());
    }

    private List<Integer> preorderTraversal(TreeNode curNode, List<Integer> result) {
        if (curNode == null) { return result; }

        result.add(curNode.val);
        preorderTraversal(curNode.left, result);
        preorderTraversal(curNode.right, result);

        return result;
    }
}