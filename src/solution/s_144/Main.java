package solution.s_144;

public class Main {
    public static void main(String[] args) {
        TreeNode node3 = new TreeNode(3);
        TreeNode node2 = new TreeNode(2);
        node2.left = node3;
        TreeNode node1 = new TreeNode(1);
        node1.right = node2;

        Solution20201027 solution = new Solution20201027();
        System.out.println(solution.preorderTraversal(node1));
    }
}
