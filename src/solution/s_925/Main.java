package solution.s_925;

public class Main {
    public static void main(String[] args) {
        Solution20201021 solution = new Solution20201021();

        String name1 = "alex";
        String typed1 = "aaleex";
        System.out.println(solution.isLongPressedName(name1, typed1));

        String name2 = "saeed";
        String typed2 = "ssaaedd";
        System.out.println(solution.isLongPressedName(name2, typed2));

        String name3 = "leelee";
        String typed3 = "lleeelee";
        System.out.println(solution.isLongPressedName(name3, typed3));

        String name4 = "laiden";
        String typed4 = "laiden";
        System.out.println(solution.isLongPressedName(name1, typed1));
    }
}
