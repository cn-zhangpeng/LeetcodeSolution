package solution.s_925;

/**
 * 解题思路：
 *      对两个字符串进行遍历，找到连续相同的字符及字符的个数。
 *      比如：name="alex", typed="aaleex"
 *      第一个字符都是 a，name 中个数为 1，typed 中个数为 2。
 *      只需要判断字符元素相等，typed 中的个数吃大于等于 name 中的个数，即表示判断通过。进行下一次的对比。
 *      假如不通过，则返回 false。
 *      最后两个字符串都比较到了结尾，则表示成功，返回 true，有其中一个字符串还有字符，表示失败。
 */
public class Solution20201021 {
    public boolean isLongPressedName(String name, String typed) {
        if (name == null || typed == null) { return false; }

        // 两个字符串的指针
        int i = 0, j = 0;
        // 当前比较的两个元素
        char nameEle = '0', typedEle = '0';
        // 当前比较的两个元素的数量
        int nameEleNum = 0, typedEleNum = 0;

        while (i < name.length() && j < typed.length()) {
            // 取到 name 字符串中当前比较的字符及数量
            for (; i < name.length(); i ++) {
                nameEle = name.charAt(i);
                nameEleNum ++;

                if (i < name.length() - 1 && nameEle != name.charAt(i + 1)) {
                    i ++;
                    break;
                }
            }

            // 取到 typed 字符串中当前比较的字符及数量
            for (; j < typed.length(); j ++) {
                typedEle = typed.charAt(j);
                typedEleNum ++;

                if (j < typed.length() - 1 && typedEle != typed.charAt(j + 1)) {
                    j ++;
                    break;
                }
            }

            // 假如两个字符不相等，或者 typed 中的数量小于 name 中的数量，则表示不符合，返回 false。
            if (nameEle != typedEle || nameEleNum > typedEleNum) {
                return false;
            }

            // 初始化统计值
            nameEle = '0';
            typedEle = '0';
            nameEleNum = 0;
            typedEleNum = 0;
        }

        // 假如两个字符串正好都到了结束的位置，表示符合，返回 true；假如有其中一个字符串不在结尾的位置，则表示不符合，返回 false。
        return (i == name.length() && j == typed.length());
    }
}
