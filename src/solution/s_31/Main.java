package solution.s_31;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Solution20201026 solution = new Solution20201026();

        int[] nums1 = new int[] {1, 2, 3};
        solution.nextPermutation(nums1);
        System.out.println(Arrays.toString(nums1));

        int[] nums2 = new int[] {3, 2, 1};
        solution.nextPermutation(nums2);
        System.out.println(Arrays.toString(nums2));

        int[] nums3 = new int[] {1, 1, 5};
        solution.nextPermutation(nums3);
        System.out.println(Arrays.toString(nums3));

        int[] nums4 = new int[] {1, 3, 2};
        solution.nextPermutation(nums4);
        System.out.println(Arrays.toString(nums4));

        int[] nums5 = new int[] {4,5,2,6,3,1};
        solution.nextPermutation(nums5);
        System.out.println(Arrays.toString(nums5));
    }
}
