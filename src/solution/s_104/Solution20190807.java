package solution.s_104;

import java.util.ArrayList;
import java.util.List;

/**
 * 递归实现
 */
public class Solution20190807 {
    public int maxDepth(TreeNode root) {
        return maxDepth(root, 1, 0);
    }

    private int maxDepth(TreeNode curNode, int depth, int maxDepth) {
        if (curNode == null) { return Math.max(depth - 1, maxDepth); }

        maxDepth = maxDepth(curNode.left, depth + 1, maxDepth);
        maxDepth = maxDepth(curNode.right, depth + 1, maxDepth);

        return maxDepth;
    }
}

class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;
    TreeNode(int x) { val = x; }
}
