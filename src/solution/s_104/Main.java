package solution.s_104;

public class Main {
    public static void main(String[] args) {
        TreeNode node15 = new TreeNode(15);
        TreeNode node7 = new TreeNode(7);
        TreeNode node20 = new TreeNode(20);
        node20.left = node15;
        node20.right = node7;
        TreeNode node9 = new TreeNode(9);
        TreeNode node3 = new TreeNode(3);
        node3.left = node9;
        node3.right = node20;

        Solution20190807 solution = new Solution20190807();
        System.out.println(solution.maxDepth(node3));
    }
}
