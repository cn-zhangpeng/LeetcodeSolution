package solution.s_11;

public class Main {
    public static void main(String[] args) {
        Solution20200814 solution = new Solution20200814();
        int[] height = {1, 8, 6, 2, 5, 4, 8, 3, 7};
        System.out.println(solution.maxArea(height));
    }
}
