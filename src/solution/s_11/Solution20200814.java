package solution.s_11;

public class Solution20200814 {
    public int maxArea(int[] height) {
        if (height.length < 2) { return 0; }

        int maxArea = 0;
        for (int i = 0; i < height.length; i ++) {
            for (int j = i + 1; j < height.length; j ++) {
                int temp = (j - i) * Math.min(height[i], height[j]);
                if (temp > maxArea) {
                    maxArea = temp;
                }
            }
        }

        return maxArea;
    }
}
