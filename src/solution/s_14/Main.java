package solution.s_14;

public class Main {
    public static void main(String[] args) {
        String[] strs1 = {"flower","flow","flight"};
        String[] strs2 = {"dog","racecar","car"};
        String[] strs3 = {"","racecar","car"};
        String[] strs4 = {"aa","a"};
        System.out.println(new Solution20190918().longestCommonPrefix(strs1));
        System.out.println(new Solution20190918().longestCommonPrefix(strs2));
        System.out.println(new Solution20190918().longestCommonPrefix(strs3));
        System.out.println(new Solution20190918().longestCommonPrefix(strs4));
    }
}
