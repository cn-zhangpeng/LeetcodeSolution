package solution.s_14;

public class Solution20190918 {
    public String longestCommonPrefix(String[] strs) {
        if (strs.length == 0) return "";
        StringBuilder res = new StringBuilder();
        int index = 0;
        Outer: while (true) {
            if (strs[0].length() <= index) break;
            char c = strs[0].charAt(index);
            for (int i = 1; i < strs.length; i ++) {
                if (strs[i].length() <= index || c != strs[i].charAt(index)) {
                    break Outer;
                }
            }
            res.append(c);
            index ++;
        }
        return res.toString();
    }
}
