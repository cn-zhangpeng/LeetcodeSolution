package solution.s_771;

public class Main {
    public static void main(String[] args) {
        System.out.println(new Solution20190918().numJewelsInStones("aA", "aAAbbbb"));
        System.out.println(new Solution20190918().numJewelsInStones("z", "ZZ"));
    }
}
