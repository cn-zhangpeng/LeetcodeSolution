package solution.s_589;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        Node node3 = new Node(3);
        node3.children = new ArrayList<Node>() {{
            add(new Node(5));
            add(new Node(6));
        }};
        Node node2 = new Node(2);
        Node node4 = new Node(4);
        Node node1 = new Node(1);
        node1.children = new ArrayList<Node>() {{
            add(node3);
            add(node2);
            add(node4);
        }};

        Solution20190807 solution = new Solution20190807();
        System.out.println(solution.preorder(node1));
    }
}
