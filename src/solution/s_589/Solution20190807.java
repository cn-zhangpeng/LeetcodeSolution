package solution.s_589;

import java.util.ArrayList;
import java.util.List;

public class Solution20190807 {
    public List<Integer> preorder(Node root) {
        List<Integer> result = new ArrayList<>();
        if (root == null) { return result; }
        return preorder(root, result);
    }

    private List<Integer> preorder(Node curNode, List<Integer> result) {
        result.add(curNode.val);

        if (curNode.children == null || curNode.children.isEmpty()) {
            return result;
        }

        for (int i = 0; i < curNode.children.size(); i ++) {
            preorder(curNode.children.get(i), result);
        }

        return result;
    }
}

class Node {
    public int val;
    public List<Node> children;

    public Node() {}

    public Node(int _val) {
        val = _val;
    }

    public Node(int _val, List<Node> _children) {
        val = _val;
        children = _children;
    }
};
