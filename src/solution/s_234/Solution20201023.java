package solution.s_234;

import java.util.ArrayList;
import java.util.List;

/**
 * 解题思路：
 *      遍历链表，将整个链表保存到列表中。
 *      然后通过双指针遍历列表，判断是否为回文数。
 */
public class Solution20201023 {
    public boolean isPalindrome(ListNode head) {
        // 转换成列表
        List<Integer> list = new ArrayList<>();
        while (head != null) {
            list.add(head.val);
            head = head.next;
        }

        // 双指针
        for (int i = 0, j = list.size() - 1; i < j; i ++, j --) {
            if (! list.get(i).equals(list.get(j))) {
                return false;
            }
        }

        return true;
    }
}

class ListNode {
    int val;
    ListNode next;
    ListNode(int x) { val = x; }
}
