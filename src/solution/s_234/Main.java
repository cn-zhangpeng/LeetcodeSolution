package solution.s_234;

public class Main {
    public static void main(String[] args) {
        Solution20201023 solution = new Solution20201023();

        ListNode n1 = constructListNode(new int[] {1, 2});
        System.out.println(solution.isPalindrome(n1));

        ListNode n2 = constructListNode(new int[] {1, 2, 2, 1});
        System.out.println(solution.isPalindrome(n2));

        ListNode n3 = constructListNode(new int[] {1, 2, 3, 2, 1});
        System.out.println(solution.isPalindrome(n3));

        ListNode n4 = constructListNode(new int[] {});
        System.out.println(solution.isPalindrome(n4));
    }

    private static ListNode constructListNode(int[] vals) {
        if (vals.length <= 0) { return null; }

        ListNode head = new ListNode(vals[0]), p = head;
        for (int i = 1; i < vals.length; i ++) {
            p.next = new ListNode(vals[i]);
            p = p.next;
        }

        return head;
    }
}
