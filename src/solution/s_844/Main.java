package solution.s_844;

public class Main {
    public static void main(String[] args) {
        Solution20201019 solution = new Solution20201019();

        String s1 = "ab#c", t1 = "ad#c";
        System.out.println(solution.backspaceCompare(s1, t1));

        String s2 = "ab##", t2 = "c#d#";
        System.out.println(solution.backspaceCompare(s2, t2));

        String s3 = "a##c", t3 = "#a#c";
        System.out.println(solution.backspaceCompare(s3, t3));

        String s4 = "a#c", t4 = "b";
        System.out.println(solution.backspaceCompare(s4, t4));
    }
}
