package solution.s_844;

import java.util.Stack;

/**
 * 解题思路：
 *      利用栈结构，分别求出 S 和 T 的最终结果，然后进行比较。
 */
public class Solution20201019 {
    public boolean backspaceCompare(String S, String T) {
        return handleStr(S).equals(handleStr(T));
    }

    private String handleStr(String str) {
        Stack<Character> sStack = new Stack<>();
        for (int i = 0; i < str.length(); i ++) {
            char c = str.charAt(i);
            if (c != '#') {
                sStack.push(c);
            } else if (sStack.size() > 0) {
                sStack.pop();
            }
        }
        return sStack.toString();
    }
}
