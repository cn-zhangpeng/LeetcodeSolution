package solution.s_98;

public class Main {
    public static void main(String[] args) {
        System.out.println(test1());
        System.out.println(test2());
        System.out.println(test3());
    }

    private static boolean test1() {
        TreeNode node1 = new TreeNode(1);
        TreeNode node3 = new TreeNode(3);
        TreeNode node2 = new TreeNode(2);
        node2.left = node1;
        node2.right = node3;

        Solution20190807 solution = new Solution20190807();
        return solution.isValidBST(node2);
    }

    private static boolean test2() {
        TreeNode node3 = new TreeNode(3);
        TreeNode node6 = new TreeNode(6);
        TreeNode node4 = new TreeNode(4);
        node4.left = node3;
        node4.right = node6;
        TreeNode node1 = new TreeNode(1);
        TreeNode node5 = new TreeNode(5);
        node5.left = node1;
        node5.right = node4;

        Solution20190807 solution = new Solution20190807();
        return solution.isValidBST(node5);
    }

    private static boolean test3() {
        TreeNode node6 = new TreeNode(6);
        TreeNode node20 = new TreeNode(20);
        TreeNode node15 = new TreeNode(15);
        node15.left = node6;
        node15.right = node20;
        TreeNode node5 = new TreeNode(5);
        TreeNode node10 = new TreeNode(10);
        node10.left = node5;
        node10.right = node15;

        Solution20190807 solution = new Solution20190807();
        return solution.isValidBST(node10);
    }
}
