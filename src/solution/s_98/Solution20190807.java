package solution.s_98;

import java.util.ArrayList;
import java.util.List;

/**
 * 递归实现
 */
public class Solution20190807 {
    public boolean isValidBST(TreeNode root) {
        if (root == null || (root.left == null && root.right == null)) { return true; }
        List<Integer> numList = preorder(root, new ArrayList<>());
        System.out.println(numList);
        for (int i = 0; i < numList.size() - 1; i ++) {
            if (numList.get(i) >= numList.get(i + 1)) {
                return false;
            }
        }
        return true;
    }

    private List<Integer> preorder(TreeNode curNode, List<Integer> result) {
        if (curNode == null) { return result; }

        result = preorder(curNode.left, result);
        result.add(curNode.val);
        result = preorder(curNode.right, result);

        return result;
    }
}

class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;
    TreeNode(int x) { val = x; }
}
