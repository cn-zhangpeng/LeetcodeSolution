package solution.s_206;

public class Main {

    public static void main(String[] args) {
        Solution20220307 solution = new Solution20220307();

        ListNode head1 = buildListNode(new int[] { 1, 2, 3, 4, 5 });
        printListNode(head1);
        printListNode(solution.reverseList(head1));
        System.out.println();

        ListNode head2 = buildListNode(new int[] { 1, 2 });
        printListNode(head2);
        printListNode(solution.reverseList(head2));
        System.out.println();

        ListNode head3 = buildListNode(new int[] { });
        printListNode(head3);
        printListNode(solution.reverseList(head3));
    }

    private static ListNode buildListNode(int[] values) {
        ListNode head = new ListNode();
        ListNode cur = head;
        for (int value : values) {
            cur.next = new ListNode(value);
            cur = cur.next;
        }
        return head.next;
    }

    private static void printListNode(ListNode head) {
        while (head != null) {
            System.out.print(head.val + " ");
            head = head.next;
        }
        System.out.println();
    }

}
