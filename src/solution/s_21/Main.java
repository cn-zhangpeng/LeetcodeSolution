package solution.s_21;

public class Main {
    public static void main(String[] args) {
        ListNode l1 = new ListNode(1);
        l1.next = new ListNode(2);
        l1.next.next = new ListNode(4);
        ListNode l2 = new ListNode(1);
        l2.next = new ListNode(3);
        l2.next.next = new ListNode(4);

        ListNode res = new Solution20190918().mergeTwoLists(l1, l2);
        System.out.println(new Main().print(res));
    }

    private String print(ListNode listNode) {
        StringBuilder res = new StringBuilder();
        for (ListNode cur = listNode; cur != null; cur = cur.next) {
            if (cur.next == null) {
                res.append(cur.val);
            } else {
                res.append(cur.val).append("->");
            }
        }
        return res.toString();
    }
}
