package solution.s_1356;

import java.util.*;
import java.util.function.ToIntFunction;

/**
 * 解题思路：
 *      首先定义一个数组，求出十进制数据中每个数值的二进制数值中 1 的个数，使用 Map 保存结果。key：数值，value：二进制中 1 的个数。
 *      然后将原十进制数组转成 List。
 *      通过自定义排序规则实现排序。
 */
public class Solution20201106 {
    public int[] sortByBits(int[] arr) {
        // 二进制中 1 的个数
        Map<Integer, Integer> bMap = new HashMap<>();

        // 原十进制数组 List
        List<Integer> numList = new ArrayList<>(arr.length);

        for (int i = 0; i < arr.length; i ++) {
            // 计算二进制中 1 的个数
            bMap.put(arr[i], getBinNum(arr[i]));
            // 原数值存入 List 中
            numList.add(arr[i]);
        }

        // 排序
        // numList.sort(Comparator.comparingInt((ToIntFunction<Integer>) bMap::get).thenComparingInt(num -> num));
        numList.sort((num1, num2) -> {
            int com = bMap.get(num1).compareTo(bMap.get(num2));
            if (com != 0) {
                return com;
            }
            return num1 - num2;
        });

        // 结果
        for (int i = 0; i < numList.size(); i ++) {
            arr[i] = numList.get(i);
        }

        return arr;
    }

    private int getBinNum(int num) {
        int count = 0;
        while (num != 0) {
            if (num % 2 == 1) {
                count ++;
            }
            num = num / 2;
        }
        return count;
    }
}
