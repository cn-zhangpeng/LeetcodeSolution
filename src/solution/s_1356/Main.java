package solution.s_1356;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Solution20201106 solution = new Solution20201106();

        int[] arr1 = new int[] {0,1,2,3,4,5,6,7,8};
        System.out.println(Arrays.toString(solution.sortByBits(arr1)));

        int[] arr2 = new int[] {1024,512,256,128,64,32,16,8,4,2,1};
        System.out.println(Arrays.toString(solution.sortByBits(arr2)));

        int[] arr3 = new int[] {10000,10000};
        System.out.println(Arrays.toString(solution.sortByBits(arr3)));

        int[] arr4 = new int[] {2,3,5,7,11,13,17,19};
        System.out.println(Arrays.toString(solution.sortByBits(arr4)));

        int[] arr5 = new int[] {10,100,1000,10000};
        System.out.println(Arrays.toString(solution.sortByBits(arr5)));
    }
}
