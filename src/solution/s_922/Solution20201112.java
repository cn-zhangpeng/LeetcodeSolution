package solution.s_922;

/**
 * 解题思路：
 *      定义两个指针 i、j，i 只遍历偶数，j 只遍历奇数。
 *      假如 A[i] 是偶数，继续往下遍历，直到找到 A[i] 为奇数。
 *      j 同样以上操作。
 *      找到 A[i]、A[j] 都不满足的条件，对两个数据进行位置交换。
 *      直到整个数组遍历完。
 */
public class Solution20201112 {
    public int[] sortArrayByParityII(int[] A) {
        if (A == null || A.length <= 1) { return A; }

        int i = 0, j = 1;
        while (i < A.length && j < A.length) {
            // 假如 i 的位置满足条件，往下进行
            if (A[i] % 2 == 0) {
                i += 2;
                continue;
            }

            // 假如 j 的位置满足条件，往下进行
            if (A[j] % 2 != 0) {
                j += 2;
                continue;
            }

            // 假如两个位置都不满足，进行位置交换
            if (A[i] % 2 != 0 && A[j] % 2 == 0) {
                int temp = A[i];
                A[i] = A[j];
                A[j] = temp;

                i += 2;
                j += 2;
            }
        }

        return A;
    }
}
