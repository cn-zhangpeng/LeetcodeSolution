package solution.s_922;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Solution20201112 solution = new Solution20201112();

        int[] nums = new int[] {4,2,5,7};
        int[] result = solution.sortArrayByParityII(nums);
        System.out.println(Arrays.toString(result));
    }
}
