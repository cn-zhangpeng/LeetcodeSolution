package solution.s_106;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Solution20200927 solution = new Solution20200927();
        int[] inOrder = {9, 3, 15, 20, 7};
        int[] postOrder = {9, 15, 7, 20, 3};
        TreeNode treeNode = solution.buildTree(inOrder, postOrder);
        levelTraversal(treeNode);
    }

    public static void levelTraversal(TreeNode treeNode) {
        List<TreeNode> nodeList = new ArrayList<>();
        nodeList.add(treeNode);

        while (nodeList.size() > 0) {
            TreeNode node = nodeList.remove(0);
            System.out.print(node.val + " ");
            if (node.left != null) {
                nodeList.add(node.left);
            }
            if (node.right != null) {
                nodeList.add(node.right);
            }
        }

        System.out.println();
    }
}
