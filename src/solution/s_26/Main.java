package solution.s_26;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Solution20200814 solution = new Solution20200814();
        int[] nums = new int[] {1, 1, 2};
        int size = solution.removeDuplicates(nums);
        System.out.println(Arrays.toString(nums));
        System.out.println(size);

        nums = new int[] {0, 0, 1, 1, 1, 2, 2, 3, 3, 4};
        size = solution.removeDuplicates(nums);
        System.out.println(Arrays.toString(nums));
        System.out.println(size);
    }
}
