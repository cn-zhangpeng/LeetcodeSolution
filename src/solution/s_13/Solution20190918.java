package solution.s_13;

import java.util.HashMap;
import java.util.Map;

public class Solution20190918 {
    private static final Map<Character, Integer> map = new HashMap<Character, Integer>() {{
        put('I', 1);
        put('V', 5);
        put('X', 10);
        put('L', 50);
        put('C', 100);
        put('D', 500);
        put('M', 1000);
    }};

    public int romanToInt(String s) {
        int sum = 0, i = 0;
        while (i < s.length()) {
            if (i == s.length() - 1) {
                sum += map.get(s.charAt(i));
                i ++;
                continue;
            }
            if (map.get(s.charAt(i)) < map.get(s.charAt(i + 1))) {
                sum += (map.get(s.charAt(i + 1)) - map.get(s.charAt(i)));
                i += 2;
                continue;
            }
            sum += map.get(s.charAt(i));
            i ++;
        }
        return sum;
    }
}
