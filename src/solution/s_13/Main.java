package solution.s_13;

public class Main {
    public static void main(String[] args) {
        System.out.println(new Solution20190918().romanToInt("III"));
        System.out.println(new Solution20190918().romanToInt("IV"));
        System.out.println(new Solution20190918().romanToInt("IX"));
        System.out.println(new Solution20190918().romanToInt("LVIII"));
        System.out.println(new Solution20190918().romanToInt("MCMXCIV"));
    }
}
