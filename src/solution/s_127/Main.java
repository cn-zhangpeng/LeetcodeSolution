package solution.s_127;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Solution20201026 solution = new Solution20201026();

        String[] dictArr1 = new String[] {"hot","dot","dog","lot","log","cog"};
        List<String> dict1 = new ArrayList<>(dictArr1.length);
        Collections.addAll(dict1, dictArr1);
        System.out.println(solution.ladderLength("hit", "cog", dict1));

        String[] dictArr2 = new String[] {"hot","dot","dog","lot","log"};
        List<String> dict2 = new ArrayList<>(dictArr1.length);
        Collections.addAll(dict2, dictArr2);
        System.out.println(solution.ladderLength("hit", "cog", dict2));

        String[] dictArr3 = new String[] {"hot","dot","tog","cog"};
        List<String> dict3 = new ArrayList<>(dictArr1.length);
        Collections.addAll(dict3, dictArr3);
        System.out.println(solution.ladderLength("hit", "cog", dict3));
    }
}
