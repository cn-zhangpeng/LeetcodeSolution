package solution.s_349;

import java.util.HashSet;
import java.util.Set;

/**
 * 解题思路：
 *      将 nums2 保存到一个 set 中，再定义一个 set 作为交集的结果。
 *      遍历 nums1，假如当前元素 nums2 set 中有。那么就添加到交集 set 中。
 *      最后将交集 set 转换为数组返回。
 */
public class Solution20201102 {
    public int[] intersection(int[] nums1, int[] nums2) {
        if (nums1.length <= 0 || nums2.length <= 0) {
            return new int[] {};
        }

        // 将 nums2 存入 set 中
        Set<Integer> nums2Set = new HashSet<>();
        for (int num : nums2) {
            nums2Set.add(num);
        }

        // 结果集合
        Set<Integer> resultSet = new HashSet<>();
        for (int num : nums1) {
            if (nums2Set.contains(num)) {
                resultSet.add(num);
            }
        }

        // set 转为数组返回
        return resultSet.stream().mapToInt(Number::intValue).toArray();
    }
}
