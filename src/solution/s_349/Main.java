package solution.s_349;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Solution20201102 solution = new Solution20201102();

        int[] nums1 = new int[] {1,2,2,1}, nums2 = new int[] {2,2};
        System.out.println(Arrays.toString(solution.intersection(nums1, nums2)));

        int[] nums3 = new int[] {4,9,5}, nums4 = new int[] {9,4,9,8,4};
        System.out.println(Arrays.toString(solution.intersection(nums3, nums4)));
    }
}
