package solution.s_143;

public class Main {
    public static void main(String[] args) {
        Solution20201020 solution = new Solution20201020();

        ListNode head1 = constructListNode(new int[] {1, 2, 3, 4});
        solution.reorderList(head1);
        printNode(head1);

        System.out.println();

        ListNode head2 = constructListNode(new int[] {1, 2, 3, 4, 5});
        solution.reorderList(head2);
        printNode(head2);
    }

    private static ListNode constructListNode(int[] vals) {
        if (vals.length <= 0) { return null; }

        ListNode head = new ListNode(vals[0]), p = head;
        for (int i = 1; i < vals.length; i ++) {
            p.next = new ListNode(vals[i]);
            p = p.next;
        }

        return head;
    }

    private static void printNode(ListNode head) {
        while (head != null) {
            System.out.printf("%d", head.val);
            if (head.next != null) {
                System.out.print("->");
            }
            head = head.next;
        }
    }
}
