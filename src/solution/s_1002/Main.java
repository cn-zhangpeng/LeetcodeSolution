package solution.s_1002;

public class Main {
    public static void main(String[] args) {
        Solution20201014 solution = new Solution20201014();

        String[] data1 = {"bella", "label", "roller"};
        System.out.println(solution.commonChars(data1));

        String[] data2 = {"cool", "lock", "cook"};
        System.out.println(solution.commonChars(data2));

        String[] data3 = {"cool"};
        System.out.println(solution.commonChars(data3));
    }
}
