package solution.s_4;

public class Main {
    public static void main(String[] args) {
        example1();
        example2();
        example3();
        example4();
    }

    private static void example1() {
        Solution20220215 solution = new Solution20220215();

        int[] nums1 = {1, 3}, nums2 = {2};
        System.out.println(solution.findMedianSortedArrays(nums1, nums2));
    }

    private static void example2() {
        Solution20220215 solution = new Solution20220215();

        int[] nums1 = {1, 2}, nums2 = {3, 4};
        System.out.println(solution.findMedianSortedArrays(nums1, nums2));
    }

    private static void example3() {
        Solution20220215 solution = new Solution20220215();

        int[] nums1 = {}, nums2 = {1};
        System.out.println(solution.findMedianSortedArrays(nums1, nums2));
    }

    private static void example4() {
        Solution20220215 solution = new Solution20220215();

        int[] nums1 = {1}, nums2 = {1};
        System.out.println(solution.findMedianSortedArrays(nums1, nums2));
    }
}
