package solution.s_1030;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * 解题思路：
 *      使用一个辅助类，将两个点做成一个对象。
 *      然后将整个矩阵中的元素放入到列表中。
 *      自定义排序规则进行排序。
 */
public class Solution20201117 {
    public int[][] allCellsDistOrder(int R, int C, int r0, int c0) {
        // 矩阵列表
        List<CustomPoint> list = new ArrayList<>();
        for (int r = 0; r < R; r ++) {
            for (int c = 0; c < C; c ++) {
                list.add(new CustomPoint(r, c));
            }
        }

        // 进行自定义排序
        list.sort(Comparator.comparingInt(n -> Math.abs(n.x - r0) + Math.abs(n.y - c0)));

        // 构造结果返回
        int[][] result = new int[R * C][2];
        for (int i = 0; i < list.size(); i ++) {
            CustomPoint cp = list.get(i);
            result[i] = new int[] { cp.x, cp.y };
        }
        return result;
    }

    public static class CustomPoint {
        int x;
        int y;

        public CustomPoint(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }
}


