package solution.s_1030;

public class Main {
    public static void main(String[] args) {
        Solution20201117 solution = new Solution20201117();

        int[][] result1 = solution.allCellsDistOrder(1, 2, 0, 0);
        printTwoDimensionArr(result1);

        int[][] result2 = solution.allCellsDistOrder(2, 2, 0, 1);
        printTwoDimensionArr(result2);

        int[][] result3 = solution.allCellsDistOrder(2, 3, 1, 2);
        printTwoDimensionArr(result3);
    }

    private static void printTwoDimensionArr(int[][] arr) {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (int i = 0; i < arr.length; i ++) {
            sb.append("[").append(arr[i][0]).append(",").append(arr[i][1]).append("]");
            if (i != arr.length - 1) {
                sb.append(",");
            }
        }
        sb.append("]");
        System.out.println(sb.toString());
    }
}
