package solution.s_1365;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Solution20201026 solution = new Solution20201026();

        int[] nums1 = new int[] {8, 1, 2, 2, 3};
        int[] result1 = solution.smallerNumbersThanCurrent(nums1);
        System.out.println(Arrays.toString(result1));

        int[] nums2 = new int[] {6,5,4,8};
        int[] result2 = solution.smallerNumbersThanCurrent(nums2);
        System.out.println(Arrays.toString(result2));

        int[] nums3 = new int[] {7,7,7,7};
        int[] result3 = solution.smallerNumbersThanCurrent(nums3);
        System.out.println(Arrays.toString(result3));
    }
}
