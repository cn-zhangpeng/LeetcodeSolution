package solution.s_463;

/**
 * 解题思路：
 *      因为岛是相连的，只需要遍历二维数组，判断值为 1 的上下左右 4 个位置。
 *      上下左右 4 个位置中，只要值为 0 或者到达边缘，就说明是边缘位置，周长就 +1。
 */
public class Solution20201030 {
    public int islandPerimeter(int[][] grid) {
        if (grid.length == 0 || grid[0].length == 0) { return 0; }

        int rows = grid.length;
        int cols = grid[0].length;

        int circumference = 0;
        for (int i = 0; i < rows; i ++) {
            for (int j = 0; j < cols; j ++) {
                if (grid[i][j] == 1) {
                    // 上下左右 4 个位置中，只要值为 0 或者到达边缘，就说明是边缘位置，周长就 +1。
                    if (i == 0 || grid[i - 1][j] == 0) { circumference ++; }
                    if (i == rows - 1 || grid[i + 1][j] == 0) { circumference ++; }
                    if (j == 0 || grid[i][j - 1] == 0) { circumference ++; }
                    if (j == cols - 1 || grid[i][j + 1] == 0) { circumference ++; }
                }
            }
        }

        return circumference;
    }
}
