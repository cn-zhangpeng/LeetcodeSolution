package solution.s_463;

public class Main {
    public static void main(String[] args) {
        Solution20201030 solution = new Solution20201030();

        int[][] grid = new int[][] {
                {0,1,0,0},
                {1,1,1,0},
                {0,1,0,0},
                {1,1,0,0}};
        System.out.println(solution.islandPerimeter(grid));

        int[][] grid2 = new int[][] {{1,0}};
        System.out.println(solution.islandPerimeter(grid2));
    }
}
