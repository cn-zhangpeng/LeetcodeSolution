package solution.s_796;

public class Main {
    public static void main(String[] args) {
        Solution20190806 main = new Solution20190806();
        System.out.println(main.rotateString("abcde", "cdeab"));
        System.out.println(main.rotateString("abcde", "abced"));
    }
}
