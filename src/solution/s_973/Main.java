package solution.s_973;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Solution20201026 solution = new Solution20201026();

        int[][] points1 = new int[][] { {1,3}, {-2,2} };
        System.out.println(Arrays.deepToString(solution.kClosest(points1, 1)));

        int[][] points2 = new int[][] { {3,3}, {5,-1}, {-2,4} };
        System.out.println(Arrays.deepToString(solution.kClosest(points2, 2)));
    }
}
