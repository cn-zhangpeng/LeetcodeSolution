package solution.s_973;

import java.util.ArrayList;
import java.util.List;

/**
 * 解题思路：
 *      创建一个 CustomPoint 类，将 points 转换成一个 List<CustomPoint>。
 *      通过自定义排序规则对 List 进行排序。
 *      将前 k 个元素组装成二维数组返回。
 */
public class Solution20201026 {
    public int[][] kClosest(int[][] points, int K) {
        if (points.length <= 0 || K <= 0) { return new int[][] {}; }

        // 组装 List
        List<CustomPoint> pointList = new ArrayList<>();
        for (int[] point : points) {
            pointList.add(new CustomPoint(point[0], point[1]));
        }

        // 进行排序
        pointList.sort((p1, p2) -> ((p1.getX() * p1.getX() + p1.getY() * p1.getY()) - (p2.getX() * p2.getX() + p2.getY() * p2.getY())));

        // 组装结果
        int[][] result = new int[K][2];
        for (int i = 0; i < K; i ++) {
            result[i] = new int[] { pointList.get(i).getX(), pointList.get(i).getY() };
        }

        return result;
    }

    private static class CustomPoint {
        private int x;
        private int y;

        public CustomPoint(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }
    }
}
